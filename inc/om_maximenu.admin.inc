<?php
// $Id$

/**
 * @file
 * OM Maximenu Admin Configuration
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 *
 */
 
/**
 * Admin Form - Simple Editing
 *
 */
function om_maximenu_admin() {
  drupal_set_title(t('OM Maximenu Settings'));
  
  $menus = array();
  $maximenus = variable_get('om_maximenu', $menus);
  
  $form = array();
  $form['om_maximenus'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  
  $maximenus_array = array();
  foreach ($maximenus as $menu_key => $menu_content) {
    $maximenus_array[$menu_key] = array(
      '#type'        => 'fieldset',
      '#title'       => $menu_content['title'],
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );  
    $menu_content['menu_key'] = $menu_key;
    $maximenus_array[$menu_key] += _om_maximenu_admin($menu_content);
  }
  $count = count($maximenus) + 1;
  $menu_content_new['title'] = 'New Menu';
  $menu_content_new['links'] = array();
  $menu_content_new['menu_key'] = $count;  
  $maximenus_array[$count] = array(
    '#type'        => 'fieldset',
    '#title'       => 'New Menu',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );  
  $maximenus_array[$count] += _om_maximenu_admin($menu_content_new);
  
  $form['om_maximenus'] += $maximenus_array; 
  

  $form['save'] = array( '#type' => 'submit', '#value' => t('Save'), );
  
  return $form;
}


/**
 * Admin Form
 *
 */
function _om_maximenu_admin($menu_content = array()) {
  
  $out = array();

  $out['title'] = array(
    '#type'  => 'textfield',
    '#title' => t('Menu Block Title'),    
    '#default_value' => $menu_content['title'],
  );
  $links = array();
  usort($menu_content['links'], 'om_sort_by_weight');
  $count = count($menu_content['links']);
  if (!isset($menu_content['links'])) $menu_content['links'] = array();
  $menu_content['links'] += _om_new_link($count);
  $link_id = 0;
  foreach ($menu_content['links'] as $link => $prop_val) {
    $link_id++;
    $links['links'][$link_id] = array(
      '#type'   => 'fieldset',
      '#title'  => $prop_val['link_title'],
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $links['links'][$link_id]['link_title'] = array(
      '#type'   => 'textfield',
      '#title'  => 'Link Title',
      '#default_value'  => trim($prop_val['link_title']),      
    );
    $links['links'][$link_id]['path'] = array(
      '#type'   => 'textfield',
      '#title'  => 'Path',
      '#default_value'  => $prop_val['path'],
    );      
    $links['links'][$link_id]['id'] = array(
      '#type'   => 'textfield',
      '#title'  => 'ID',
      '#default_value'  => $prop_val['id'],
    );
    $links['links'][$link_id]['class'] = array(
      '#type'   => 'textfield',
      '#title'  => 'Classes',
      '#default_value'  => $prop_val['class'],
    );
    $links['links'][$link_id]['description'] = array(
      '#type'   => 'textfield',
      '#title'  => 'Description',
      '#default_value'  => $prop_val['description'],
    );
    $links['links'][$link_id]['content'] = array(
      '#type'   => 'hidden',
      '#default_value'  => $prop_val['content'],
    );
    $links['links'][$link_id]['weight'] = array(
      '#type'   => 'weight',
      '#title'  => 'Weight',
      '#delta' => 20,        
      '#default_value'  => $prop_val['weight'],
      '#suffix' => (!empty($prop_val['link_title']) && ($prop_val['link_title'] != 'New Link')) ? '<a href="' . base_path() . 'admin/settings/om-maximenu/blocks/' . $menu_content['menu_key'] . '/' . $link_id . '?destination=admin/settings/om-maximenu">Edit Content</a>': '',
    );
  }
  
  $out += $links;

  return $out;
}


/**
 * OM New Links
 *
 */
function _om_new_link($count = 0) {
  $out = array();
  $num = $count + 5;
  for ($i = $count; $i < $num; $i++) {
    $out[$i] = array(
      'path' => '',
      'link_title' => 'New Link',
      'id' => '',
      'class' => '',
      'description' => '',
      'weight' => 20,  
    );
  }
  return $out;
}

/**
 * 1 Submit for all settings
 *
 */
function om_maximenu_admin_submit($form, $form_state) {
  $om_maximenu_array = $form_state['values']['om_maximenus'];
  //dsm($om_maximenu_array); 
  $om_maximenu = array();
  foreach ($om_maximenu_array as $menu_id => $maximenu_block_content) {
    if (!empty($maximenu_block_content['title']) && ($maximenu_block_content['title'] != 'New Menu')) {
      $om_maximenu[$menu_id]['title'] = $maximenu_block_content['title'];
      foreach ($maximenu_block_content['links'] as $link => $link_prop) {
        if (!empty($link_prop['link_title']) && ($link_prop['link_title'] != 'New Link')) {
          $om_maximenu[$menu_id]['links'][$link] = array(
            'path'        => $link_prop['path'],
            'link_title'  => $link_prop['link_title'],
            'id'          => $link_prop['id'],
            'class'       => $link_prop['class'],
            'description' => $link_prop['description'],
            'content'     => $link_prop['content'],
            'weight'      => $link_prop['weight'],
          );
        }
      }
    }
  }

  drupal_set_message(t('Your settings have been saved.'));
  
  // Save all settings in 1 variable
  variable_set('om_maximenu', $om_maximenu);
} 


/**
 * Adding Blocks
 *
 */
function om_maximenu_blocks_get() {
  drupal_set_title(t('OM Maximenu Blocks'));
  $args = arg();

  $maximenus = variable_get('om_maximenu', $menus);
  $menu_title = $maximenus[$args[4]]['title'];
  $link_title = $maximenus[$args[4]]['links'][$args[5]]['link_title'];
  $blocks_enabled = explode(',', $maximenus[$args[4]]['links'][$args[5]]['content']);
  array_pop($blocks_enabled);  
  //dsm($blocks_enabled);
  
  $maxi_block_enabled = array();
  $enabled = array();
  foreach ($blocks_enabled as $bkey => $block_enabled) {
    $enabled = explode('___', trim($block_enabled));  
    $maxi_block_enabled[$enabled[0]][$enabled[1]] = 1;  
  }
  //dsm($maxi_block_enabled);

  $theme_default = variable_get('theme_default', 'garland');

  // default, module generated blocks
  $result = db_query("SELECT module, delta, title FROM {blocks} WHERE theme = '%s' ORDER BY module, delta ASC", $theme_default);
  
  while ($record = db_fetch_object($result)) {
    if ($record->module != 'om_maximenu') {
      $block  = module_invoke($record->module, 'block', 'view', $record->delta); 
      $title  = $record->title; 
      $module = $record->module;
      $delta  = $record->delta;    
      $modules[$module][$delta]['checked'] = isset($maxi_block_enabled[$module][$delta]) ? $maxi_block_enabled[$module][$delta]: 0; 
      //print $maxi_block_enabled[$module][$delta];         
      $modules[$module][$delta]['title'] = empty($title) ? empty($block['subject']) ? ucwords(preg_replace('/_/', ' ', $module)) . ' ' . $delta: $block['subject']: $title;          
    }
  }
  //dsm($modules);
  $form = array();  
  $form['om_maximenu_content'] = array(
    '#type'        => 'fieldset',
    '#title'       => t($menu_title . ' / ' . $link_title),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#tree'        => TRUE,
  );
  
  foreach ($modules as $module => $blocks) {
    $form['om_maximenu_content'][$module] = array(
      '#type'        => 'fieldset',
      '#title'       => t(ucwords(preg_replace('/_/', ' ', $module))),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
      '#tree'        => TRUE,
    );
    foreach ($blocks as $delta => $value) {
      $form['om_maximenu_content'][$module][$module . '___' . $delta] = array(
        '#type'          => 'checkbox',
        '#title'         => $value['title'], 
        '#default_value' => $value['checked'],   
      );     
    }
  }   
   
  $form['save'] = array( '#type' => 'submit', '#value' => t('Save'), );
    
  return $form;
}  


/**
 * Adding Blocks Submit
 *
 */
function om_maximenu_blocks_get_submit($form, $form_state) {
  $args = arg();

  $maximenu_content = $form_state['values']['om_maximenu_content'];
  //dsm($maximenu_content);
  $content = '';
  foreach ($maximenu_content as $key => $blocks) {
    foreach ($blocks as $block => $value) {
      if ($value) $content .= $block . ', ';
    }
  }
  //dsm($content);
  $om_maximenu = variable_get('om_maximenu', '');
  $om_maximenu[$args[4]]['links'][$args[5]]['content'] = $content;
  //dsm($om_maximenu);
  drupal_set_message(t('Your settings have been saved.'));
  
  // Save all settings in 1 variable
  variable_set('om_maximenu', $om_maximenu);
} 


/**
 * Sorter by weight
 *
 */
function om_sort_by_weight($a, $b) {
  return $a['weight'] - $b['weight'];
}


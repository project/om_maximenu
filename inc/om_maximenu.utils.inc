<?php
// $Id$

/**
 * @file
 * OM Maximenu Admin Utilities
 *
 * @author: Daniel Honrade http://drupal.org/user/351112
 *
 */
 
/**
 * Load all enabled blocks
 *
 */
function om_maximenu_load($maximenu_delta = '') {
  $maximenu_delta = explode('-', $maximenu_delta);
  //$maximenu_delta = 'om-maximenu-1';
  $delta = $maximenu_delta[2];
  if ($delta != 0) {
    $out = array();
    $maximenus = variable_get('om_maximenu', $menus);
    $out['title'] = $maximenus[$delta]['title'];
    $out['content'] = om_maximenu_links_render($maximenus[$delta]['links']);
    
    return $out;
  }
}


/**
 * OM Maximenu links rendering engine
 *
 */
function om_maximenu_links_render($links = array()) {
  $count = 0;
  $total = count($links);
  $out = '<div class="om-maximenu">';
  $out .= '<ul class="menu">';  
  foreach ($links as $key => $content) {
    $count++;
    ($count == 1) ? $classes = ' first': $classes = '';
    if ($count == $total) $classes .= ' last';   
    if ($_GET['q'] == $content['path']) $classes .= ' active';         
    $out .= '<li class="leaf' . $classes . '">';
    !empty($content['path']) ? $path = $content['path']: $path = '';
    $out .= l($content['link_title'], $path, array('attributes' => array('id' => $content['id'], 'class' => $content['class'])));
    $out .= om_maximenu_content_render($content['content']);    
    $out .= '</li>';
  }
  $out .= '</ul>';  
  
  $out .= '</div>';  
  return $out;
}


/**
 * OM Maximenu content rendering engine
 *
 */
function om_maximenu_content_render($content = '') {

  if (!empty($content)) {
    $blocks_enabled = explode(',', $content);
    $maxi_block_enabled = array();
    $enabled = array();
    foreach ($blocks_enabled as $bkey => $block_enabled) {
      $enabled = explode('___', trim($block_enabled));  
      $maxi_block_enabled[$enabled[0]] = $enabled[1];  
    }
    $out = '<div class="om-maximenu-content">';
    $out .= '<table>';  
    $out .= '<tr><td class="top top-left"></td><td class="top top-middle"></td><td class="top top-right"></td></tr>';
    $out .= '<tr><td class="middle middle-left"></td><td class="middle middle-middle">';      
    foreach ($maxi_block_enabled as $module => $delta) {
      $block = module_invoke($module, 'block', 'view', $delta);    
      $out .= $block['content'];    
    }
    $out .= '</td><td class="middle middle-right"></td></tr>';      
    $out .= '<tr><td class="bottom bottom-left"></td><td class="bottom bottom-middle"></td><td class="bottom bottom-right"></td></tr>';    
    $out .= '</table>';
    $out .= '</div>'; 
    return $out;
  } 
}




